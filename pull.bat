@rem вытягивает все подпроекты из удаленного репозитория

git pull origin

cd jhermes
git pull origin

cd ..\public
git pull origin

cd ..\verna-commons
git pull origin

cd ..\jhermes-db
git pull origin

cd ..\apidocs
git pull origin

pause
