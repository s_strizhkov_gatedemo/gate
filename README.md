### How to get a working copy of the project

1. Get correct structure of sources:
```console
cd <PROJECTS_DIR>
git clone https://gitlab.com/s_strizhkov_gatedemo/gate.git
cd gate
git clone https://gitlab.com/s_strizhkov_gatedemo/jhermes.git
git clone https://gitlab.com/s_strizhkov_gatedemo/jhermes-db.git
git clone https://gitlab.com/s_strizhkov_gatedemo/public.git
git clone https://gitlab.com/s_strizhkov_gatedemo/verna-commons.git
```
2. Set a correct deployment environment.
In `gate/environments/dev.gradle` write a settings of an application server 
(the project is set to be deployed at Glassfish) and Postgres DB. 

3. To deploy a project, call a command in `gate` directory:
```
gradlew deploy
```

4. After deploying testing is available:
```
gradlew test
```
