1. Файл postgresql-42.2.2.jar положить в папку GLASSFISH_HOME/glassfish/domains/domain1/lib

2. В файл GLASSFISH_HOME/glassfish/domains/domain1/config/domain.xml добавить:

2.1:
    <jdbc-connection-pool datasource-classname="org.postgresql.ds.PGSimpleDataSource" steady-pool-size="2" name="JHermesDbLocalPool" res-type="javax.sql.DataSource">
      <property name="password" value="*** пароль от локальной БД ***"></property>
      <property name="databaseName" value="jhermes"></property>
      <property name="serverName" value="localhost"></property>
      <property name="user" value="jhermes"></property>
      <property name="portNumber" value="5432"></property>
    </jdbc-connection-pool>
    
2.2: 
    <jdbc-resource pool-name="JHermesDbLocalPool" jndi-name="jdbc/jhermes"></jdbc-resource>
